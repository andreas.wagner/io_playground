#include <string>
#include <iostream>
#include <linux/aio_abi.h>
#include <unistd.h>
#include <sys/syscall.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdexcept>
#include <functional>
#include <list>
#include <map>
#include <utility>
#include <cstring>
#include <memory>
#include <cstdint>
#include <atomic>
#include <errno.h>
  
#include "io.h"
  
int io_setup(unsigned int nr_events, aio_context_t *ctx_idp)
{
  return syscall(SYS_io_setup, nr_events, ctx_idp);
}

int io_submit(aio_context_t ctx_id, long nr, struct iocb **iocbpp)
{
  return syscall(SYS_io_submit, ctx_id, nr, iocbpp);
}

int io_getevents(aio_context_t ctx_id,
                 long min_nr,
                 long nr,
                 struct io_event *events,
                 struct timespec *timeout)
{
  return syscall(SYS_io_getevents,
                 ctx_id,
                 min_nr,
                 nr,
                 events,
                 timeout);
}

int io_destroy(aio_context_t ctx_id)
{
  return syscall(SYS_io_destroy, ctx_id);
}

Buffer::Buffer(size_t size_):
    buffer(new std::atomic<char>[size_]),
    size(size_)
  {
  }
  
Buffer::~Buffer()
  {
    delete[] buffer;
  }
  
void Buffer::putToBuf(std::string s)
  {
    if(s.length() < size)
    {
      std::memcpy((void *) buffer, s.data(),s.length());
      used = s.length();
    }
    else
      throw std::runtime_error("String too large for buffer.");
  }
  
  std::atomic<char> * Buffer::getBuffer()
  {
    return buffer;
  }
  
  size_t Buffer::getUsedBytes()
  {
    return used;
  }
  
  void Buffer::setUsedBytes(size_t b)
  {
    used = b;
  }
  

IOContext::IOContext(unsigned int nr) :
  iocb_counter(0),
  max_iocb(nr),
  unique_number(0)
  {
    std::memset(&ctx_id, 0, sizeof(ctx_id));
    iocbs = (struct iocb**) calloc(nr, sizeof(struct iocb*));
    if(io_setup(nr, &ctx_id) != 0)
    {
      switch(errno)
      {
      case EAGAIN:
      	std::cerr << "EAGAIN" << std::endl;
      	break;
      case EFAULT:
      	std::cerr << "EFAULT" << std::endl;
      	break;
      case EINVAL:
      	std::cerr << "EINVAL" << std::endl;
      	break;
      case ENOMEM:
      	std::cerr << "ENOMEM" << std::endl;
      	break;
      case ENOSYS:
      	std::cerr << "ENOSYS" << std::endl;
      	break;
      default:
      	std::cerr << "default (?!)" << std::endl;
      }
      throw std::runtime_error("Could not setup io-context. io_setup() failed.");
    }
  }
  
IOContext::~IOContext()
  {
    free(iocbs);
    if(io_destroy(ctx_id) != 0)
    {
      std::cerr << "Could not free io-context. io_destroy() failed." << std::endl;
      exit(1);
    }
  }
  
int IOContext::processIOEvents()
  {
    std::list<std::uint64_t> execute_from_readstack{};
    
    std::list<std::uint64_t> execute_from_writestack{};

    struct io_event events[max_iocb];
    struct timespec timeout;
    timeout.tv_sec = 0;
    timeout.tv_nsec = 10000000;
    
    int count = io_getevents(ctx_id, 1, max_iocb, events, &timeout);
    for(int i = 0; i < count; i++)
    {
      switch(((struct iocb*) (events[i].obj))->aio_lio_opcode)
      {
        case IOCB_CMD_PREAD:
          if(events[i].res != -EAGAIN)
          {
            execute_from_readstack.emplace_back(events[i].data);
            read_callstack[events[i].data].second->setUsedBytes(events[i].res);
            delete (struct iocb*) (events[i].obj);
          }
          else
          {
            iocbs[iocb_counter] = (struct iocb*) events[i].obj;
            iocb_counter++;
          }
          break;
        case IOCB_CMD_PWRITE:
          if(events[i].res != -EAGAIN)
          {
            execute_from_writestack.emplace_back(events[i].data);
            delete (struct iocb*) (events[i].obj);
          }
          else
          {
            iocbs[iocb_counter] = (struct iocb*) events[i].obj;
            iocb_counter++;
          }
          break;
        default:
          //delete (struct iocb*) (events[i].obj);
          throw std::runtime_error(
            std::string("IOCB_CMD ")
            + std::to_string(((struct iocb*) (events[i].obj))->aio_lio_opcode)
            + " not implemented.");
      }
      
    }
    
//    int my_count = execute_from_readstack.size() + execute_from_writestack.size();
    for(auto i : execute_from_readstack)
    {
      read_callstack[i].first(read_callstack[i].second);
      read_callstack.erase(i);
    }
    execute_from_readstack.clear();
    for(auto i : execute_from_writestack)
    {
      write_callstack[i].first(write_callstack[i].second);
      write_callstack.erase(i);
    }
    execute_from_writestack.clear();
//    if(my_count == 0)
//    {
//      usleep(1000);
//    }
    return 0;
  }
  
  int IOContext::submitEvents()
  {
    io_submit(ctx_id, iocb_counter, iocbs);
    iocb_counter = 0;
    return 0;
  }
  
  int IOContext::registerRead(int fd, off_t offset, size_t bytes_to_read, std::shared_ptr<Buffer> lbuf, std::function<void(std::shared_ptr<Buffer>)> fn)
  {
    if(iocb_counter >= max_iocb)
      return -1;
    struct iocb *ptr = new struct iocb;
    
    ptr->aio_lio_opcode = IOCB_CMD_PREAD;
    ptr->aio_reqprio = 0;
    ptr->aio_fildes = fd;
    ptr->aio_buf = (std::uint64_t) lbuf->getBuffer();
    ptr->aio_nbytes = bytes_to_read;
    ptr->aio_offset = offset;
    iocbs[iocb_counter] = ptr;
    iocb_counter++;
    
    read_callstack[unique_number] = 
      std::pair<
        std::function<void(std::shared_ptr<Buffer>)>,
        std::shared_ptr<Buffer>>(fn, lbuf);
    
    ptr->aio_data = unique_number;
    unique_number++;
    
    return 0;
  }
  
  int IOContext::registerWrite(int fd, off_t offset, size_t bytes_to_read, std::shared_ptr<Buffer> lbuf, std::function<void(std::shared_ptr<Buffer>)> fn)
  {
    if(iocb_counter >= max_iocb)
      return -1;
    struct iocb *ptr = new struct iocb;
    
    ptr->aio_lio_opcode = IOCB_CMD_PWRITE;
    ptr->aio_reqprio = 0;
    ptr->aio_fildes = fd;
    ptr->aio_buf = (std::uint64_t) lbuf->getBuffer();
    ptr->aio_nbytes = bytes_to_read;
    ptr->aio_offset = offset;
    iocbs[iocb_counter] = ptr;
    iocb_counter++;
    
    write_callstack[unique_number] =
      std::pair<
        std::function<void(std::shared_ptr<Buffer>)>,
        std::shared_ptr<Buffer>>(fn, lbuf);
    
    ptr->aio_data = unique_number;
    unique_number++;
    
    return 0;
  }
  
  std::uint64_t IOContext::unprocessed()
  {
    return read_callstack.size() + write_callstack.size();
  }
  


