#include "io.h"

#include <sys/socket.h>
#include <netinet/in.h>
#include <time.h>
#include <signal.h>

#include <set>

std::set<int> peer_fds;
//std::set<int> wait_for_read_fds;
std::map<int, std::shared_ptr<Buffer>> readBuffers;
std::map<int, std::shared_ptr<Buffer>> writeBuffers;


void register_write(IOContext & io, int peer_fd, std::shared_ptr<Buffer> buf);

void register_read(IOContext & io, int peer_fd, std::shared_ptr<Buffer> buf)
{
  std::cerr << "*";
  readBuffers[peer_fd] = buf;
  if(writeBuffers.count(peer_fd) > 0)
    writeBuffers.erase(peer_fd);
  io.registerRead(
    peer_fd,
    0,
    1024,
    readBuffers[peer_fd],
    [&io, peer_fd](std::shared_ptr<Buffer> buf)
      {
        std::cerr << "+";
        register_write(io, peer_fd, buf);
      }
    );
}

void register_write(IOContext & io, int peer_fd, std::shared_ptr<Buffer> buf)
{
  std::cerr << "w";
  writeBuffers[peer_fd] = buf;
  readBuffers.erase(peer_fd);
  io.registerWrite(
    peer_fd,
    0,
    buf->getUsedBytes(),
    buf,
    [&io, peer_fd](std::shared_ptr<Buffer> buf)
      {
        std::cerr << "x";
        register_read(io, peer_fd, buf);
      }
  );
}

void register_accept(IOContext & io, int net_fd, std::shared_ptr<Buffer> dummy_buffer);

void register_accept(IOContext & io, int net_fd, std::shared_ptr<Buffer> dummy_buffer)
{
  io.registerRead(
    net_fd,
    0,
    0,
    dummy_buffer,
    [&io, net_fd](std::shared_ptr<Buffer> b)
    {
      std::shared_ptr<Buffer> buf = std::make_shared<Buffer>(1024);
      buf->putToBuf(std::string("Welcome!"));
      std::cerr << "a";
      sockaddr_in6 *peer;
      socklen_t socklen;
      int peer_fd = accept(net_fd, (sockaddr*) peer, &socklen);

      peer_fds.insert(peer_fd);
//    readBuffers[peer_fd] = std::make_shared<Buffer>(1024);
      register_write(
        io,
        peer_fd,
        buf
      );
      register_accept(io, net_fd, b);
    }
  );
}

volatile bool cont = true;
void sigint_handler(int)
{
  cont = false;
}



int main(int argc, char *argv[])
{
  IOContext io(16);
  signal(SIGINT, sigint_handler);
  
  std::shared_ptr<Buffer> buf1 = std::make_shared<Buffer>(1024);
  std::shared_ptr<Buffer> buf2 = std::make_shared<Buffer>(1024);
  
  buf1->putToBuf(std::string("Test123"));
  buf2->putToBuf(std::string("Test234"));
  
  int fd1 = open("test1.txt", O_CREAT | O_RDWR, 0666);
  int fd2 = open("test2.txt", O_CREAT | O_RDWR, 0666);
  
  io.registerWrite(
    fd1,
    0,
    buf1->getUsedBytes(),
    buf1,
    [](std::shared_ptr<Buffer> buf)
    {
      std::cerr << "1 written" << std::string((char *)buf->getBuffer(), buf->getUsedBytes()) << std::endl;
    });
  io.registerWrite(
    fd2,
    0,
    buf2->getUsedBytes(),
    buf2,
    [](std::shared_ptr<Buffer> buf)
    {
      std::cerr << "2 written" << std::string((char *)buf->getBuffer(), buf->getUsedBytes())  << std::endl;
    });
  
  io.submitEvents();
  while(io.unprocessed() > 0)
  {
    io.processIOEvents();
  }
  
  lseek(fd1, 0, SEEK_SET);
  lseek(fd2, 0, SEEK_SET);
  
  io.registerRead(
    fd1,
    0,
    1024, 
    buf1,
    [](std::shared_ptr<Buffer> buf)
    {
      std::cerr << std::string((char *)buf->getBuffer(), buf->getUsedBytes()) << std::endl;
    }
  );
  io.registerRead(
    fd2,
    0,
    1024, 
    buf2,
    [](std::shared_ptr<Buffer> buf)
    {
      std::cerr << std::string((char *)buf->getBuffer(), buf->getUsedBytes()) << std::endl ;
    }
  );

  io.submitEvents();
  while(io.unprocessed() > 0)
  {
    io.processIOEvents();
  }
  
  close(fd1);
  close(fd2);
  
  sockaddr_in6 in_dat;
  in_dat.sin6_family = AF_INET6;
  in_dat.sin6_port = htons(10000);
  in_dat.sin6_flowinfo = time(NULL); // FIXME: DO NOT REUSE FLOW LABEL
  std::memcpy((void*)&in_dat.sin6_addr, (void*)&in6addr_any, sizeof(in6addr_any));
  in_dat.sin6_scope_id = 0; // TODO: Is that right?
  
  int net_fd = socket(AF_INET6, SOCK_STREAM, 0);
  int opt = 1;
  if (net_fd == -1 || setsockopt(net_fd, SOL_SOCKET, SO_REUSEADDR | SO_REUSEPORT, &opt, sizeof(opt)) == -1)
  {
    std::cout << "Failed to attach socket.\n";
    return 1;
  }
  
//  fcntl(net_fd, F_SETFL, fcntl(net_fd, F_GETFL, 0)|O_NONBLOCK);
  int bind_retval = bind(net_fd, (const sockaddr*) &in_dat, sizeof(in_dat));
  if(bind_retval != 0)
    throw std::runtime_error("bind() failed");
  int listen_retval = listen(net_fd, 5);
  if(listen_retval != 0)
    throw std::runtime_error("listen() failed");
  
  
  std::shared_ptr<Buffer> dummy = std::make_shared<Buffer>(16);
  register_accept(io, net_fd, dummy);
  while(cont)
  {
    io.submitEvents();
    io.processIOEvents();
  }
  for(int fd : peer_fds)
  {
    close(fd);
  }
  close(net_fd);
  return 0;
}
