#include <string>
#include <iostream>
#include <linux/aio_abi.h>
#include <unistd.h>
#include <sys/syscall.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdexcept>
#include <functional>
#include <list>
#include <map>
#include <utility>
#include <cstring>
#include <memory>
#include <cstdint>
#include <atomic>
/*  
int io_setup(unsigned int nr_events, aio_context_t *ctx_idp)
{
  return syscall(SYS_io_setup, nr_events, ctx_idp);
}

int io_submit(aio_context_t ctx_id, long nr, struct iocb **iocbpp)
{
  return syscall(SYS_io_submit, ctx_id, nr, iocbpp);
}

int io_getevents(aio_context_t ctx_id,
                 long min_nr,
                 long nr,
                 struct io_event *events,
                 struct timespec *timeout)
{
  return syscall(SYS_io_getevents,
                 ctx_id,
                 min_nr,
                 nr,
                 events,
                 timeout);
}

int io_destroy(aio_context_t ctx_id)
{
  return syscall(SYS_io_destroy, ctx_id);
}
*/
class Buffer
{
  public:
  
  Buffer(size_t size_);
  
  ~Buffer();
  
  void putToBuf(std::string s);
  
  std::atomic<char> * getBuffer();
  
  size_t getUsedBytes();
  
  void setUsedBytes(size_t b);
  
  private:
  std::atomic<char> * buffer;
  size_t size;
  size_t used;
};

class IOContext
{
  public:
  
  IOContext(unsigned int nr);
  
  ~IOContext();
  
  int processIOEvents();
  
  int submitEvents();
  
  int registerRead(int fd, off_t offset, size_t bytes_to_read, std::shared_ptr<Buffer> lbuf, std::function<void(std::shared_ptr<Buffer>)> fn);
  
  int registerWrite(int fd, off_t offset, size_t bytes_to_read, std::shared_ptr<Buffer> lbuf, std::function<void(std::shared_ptr<Buffer>)> fn);
  
  std::uint64_t unprocessed();
  
  private:
  aio_context_t ctx_id;
  
  std::map<
    std::uint64_t,
    std::pair<
      std::function<void(std::shared_ptr<Buffer>)>,
      std::shared_ptr<Buffer>
    >
  > read_callstack;
  
  std::map<
    std::uint64_t,
    std::pair<
      std::function<void(std::shared_ptr<Buffer>)>,
      std::shared_ptr<Buffer>
    >
  > write_callstack;
  
  int iocb_counter;
  std::uint64_t unique_number;
  int max_iocb;
  struct iocb **iocbs;
};